<?php

class Node{
    public $id;
    public $parent_id;
    public $name;

    /**
     * Node constructor.
     * @param $id
     * @param $parent_id
     * @param $name
     */
    public function __construct($id, $parent_id, $name)
    {
        $this->id = $id;
        $this->parent_id = $parent_id;
        $this->name = $name;
    }

}
function getLines($file) {
    $f = fopen($file, 'r');
    $nodes = [];
    try {
        while ($line = fgets($f)) {
            if(empty(trim($line))) continue;
            $attributes = explode('|', $line);
            $nodes[] = new Node(...$attributes);
        }
        return $nodes;
    } finally {
        fclose($f);
    }
}

function createTreeView(array $nodes, $currentParentId = 0, $currentLevel = 0, $prevLevel = -1)
{
    /** @var Node $node */
    foreach ($nodes as $node){
        if($currentParentId == $node->parent_id){
            echo str_repeat("-", $currentLevel).$node->name;
            if($currentLevel > $prevLevel){
                $prevLevel = $currentLevel;
            }
            $currentLevel++;
            createTreeView($nodes, $node->id, $currentLevel, $prevLevel);
            $currentLevel--;
        }
    }
}
echo "Output data:\n";
createTreeView(getLines('data.txt'));